resource "local_file" "file" {
    content     = var.file_content
    filename = "${path.module}/hello.txt"
}
